package cz.vse.java.xname00.adventuracv.main;

import cz.vse.java.xname00.adventuracv.gui.PozorujiciPanelBatohu;
import cz.vse.java.xname00.adventuracv.gui.PozorujiciPanelVychodu;
import cz.vse.java.xname00.adventuracv.gui.PozorujiciPlanekHry;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AdventuraZaklad extends Application {

    private ImageView viewVlka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/vlk.png"),
            100.0, 100.0, false, false));
    private ImageView viewKarkulky ;

    private final IHra hra = Hra.getSingleton();
    private final TextField uzivatelskyVstup = new TextField();
    private TextArea konzole;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        BorderPane platno = new BorderPane();

        konzole = pripravKonzoli();
        HBox spodniBox = pripravSpodniBox();

        VBox panelBatohu = new PozorujiciPanelBatohu().getVBox();
        Node panelVychodu = new PozorujiciPanelVychodu().getListView();

        platno.setCenter(konzole);
        platno.setBottom(spodniBox);
        platno.setLeft(panelBatohu);
        platno.setRight(panelVychodu);

        viewKarkulky = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/karkulka.png"),
                100.0, 100.0, false, false));

        PozorujiciPlanekHry observablePlanekHry = new PozorujiciPlanekHry(viewKarkulky);
        System.out.println("Vypisuji observablePlanekhry: " + observablePlanekHry);
        AnchorPane planekHry = observablePlanekHry.getPlanekHry();
        platno.setTop(planekHry);

        planekHry.getChildren().addAll(viewVlka);
        AnchorPane.setLeftAnchor(viewVlka, 300.0);
        AnchorPane.setTopAnchor(viewVlka, 50.0);
        viewVlka.setBlendMode(BlendMode.COLOR_BURN);

        MenuBar menuBar = pripravMenu();
        VBox menuAHraVBox = new VBox();
        menuAHraVBox.getChildren().addAll(menuBar, platno);

        Scene scene = new Scene(menuAHraVBox);
        scene.getStylesheets().addAll("/zdroje/stylesheet.css");

        animujVlka();
        animujKarkulku(scene);

        uzivatelskyVstup.requestFocus();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void animujKarkulku(Scene scene){
        double karkulkaRychlost = 300.0; // pixels per unit of this computer's clock.

        LongProperty lastUpdateTime = new SimpleLongProperty(0);
        DoubleProperty karkulkyPohybX = new SimpleDoubleProperty(0.0);
        DoubleProperty karkulkyPohybY = new SimpleDoubleProperty(0.0);

        AnimationTimer karkulkaAnimation = new AnimationTimer() {
        @Override
        public void handle(long timestamp) {
            if (lastUpdateTime.get() > 0) {
                double elapsedSeconds = (timestamp - lastUpdateTime.get()) /
                        1_000_000_000.0;

                // karkulkyPohybX.get() je příliš vysoké číslo, resp. je to počet pixelů za jednotku něčeho.
                // to něco je vnitřní frekvence počítače.
                // proto to v předchozím příkazu dělíme jedním GIGA Hertzem (bez měrných jednotek) a vypadá to aspoň nějak.
                double zmenaX = elapsedSeconds * karkulkyPohybX.get();
                double souradniceX = viewKarkulky.getTranslateX();

                double zmenaY = elapsedSeconds * karkulkyPohybY.get();
                double souradniceY = viewKarkulky.getTranslateY();

                viewKarkulky.setTranslateX(souradniceX + zmenaX);
                viewKarkulky.setTranslateY(souradniceY + zmenaY);
            }
            lastUpdateTime.set(timestamp);
        }
    };
        karkulkaAnimation.start();

        scene.setOnKeyPressed(event -> {
        if (event.getCode() == KeyCode.A) {
            karkulkyPohybX.set(-karkulkaRychlost);
        }
        if (event.getCode() == KeyCode.D) {
            karkulkyPohybX.set(karkulkaRychlost);
        }
        if (event.getCode() == KeyCode.W) {
            karkulkyPohybY.set(-karkulkaRychlost);
        }
        if (event.getCode() == KeyCode.S) {
            karkulkyPohybY.set(karkulkaRychlost);
        }
    });

        scene.setOnKeyReleased(event -> {
        karkulkyPohybX.set(0.0);
        karkulkyPohybY.set(0.0);
    });
}

    private void animujVlka(){
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(viewVlka);

        translate.setByX(-150.0);
        translate.setByY(150.0);


        translate.setDuration(Duration.millis(2000));

        translate.setAutoReverse(true);
        translate.setCycleCount(TranslateTransition.INDEFINITE);

        translate.play();


        RotateTransition rotate = new RotateTransition();
        rotate.setNode(viewVlka);

        rotate.setDuration(Duration.millis(2000));

        rotate.setCycleCount(TranslateTransition.INDEFINITE);

        rotate.setByAngle(180.0);
        rotate.play();
    }

    private HBox pripravSpodniBox() {
        HBox spodniBox = new HBox();
        Label popisek = new Label("Zadej příkaz:\t");
        spodniBox.getChildren().addAll(popisek, uzivatelskyVstup);
        class UzivatelskyVstupEventHandler implements EventHandler {
            @Override
            public void handle(Event event) {
                String vstup = uzivatelskyVstup.getText();
                String odpovedHry = hra.zpracujPrikaz(vstup);
                konzole.appendText("\n");
                konzole.appendText(odpovedHry);
                konzole.appendText("\n");
            }
        }
        uzivatelskyVstup.setOnAction(new UzivatelskyVstupEventHandler());
        return spodniBox;
    }

    private TextArea pripravKonzoli() {
        TextArea konzole = new TextArea();
        String uvitani = hra.vratUvitani() + "\n";
        konzole.setText(uvitani);
        konzole.setEditable(false);
        return konzole;
    }

    private MenuBar pripravMenu() {
        MenuBar menuBar = new MenuBar();
        Menu souborMenu = new Menu("Soubor");
        Menu napovedaMenu = new Menu("Nápověda");

        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));

        MenuItem novaHraMenuItem = new MenuItem("Nová Hra", novaHraIkonka);
        novaHraMenuItem.setOnAction(event -> {
            start(new Stage());
        });

        MenuItem konecMenuItem = new MenuItem("Konec");
        konecMenuItem.setOnAction(event -> System.exit(0));

        MenuItem oAplikaciMenuItem = new MenuItem("O Aplikaci");
        MenuItem napovedaMenuItem = new MenuItem("Nápověda");

        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        // oddělovač
        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();

        souborMenu.getItems().addAll(novaHraMenuItem, separatorMenuItem, konecMenuItem);
        napovedaMenu.getItems().addAll(oAplikaciMenuItem, napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//	... set title, content,...
            alert.setTitle("Java FX Adventura");
            alert.setHeaderText("Header Text - Java FX Adventura");
            alert.setContentText("Verze ZS 2021");
            alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource
                    ("/zdroje/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
        return menuBar;
    }
}
