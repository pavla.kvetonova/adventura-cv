package cz.vse.java.xname00.adventuracv.observer;

public interface SubjectOfChange {

    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();

}
